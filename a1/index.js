let posts = [];
let count = 1;



document.querySelector("#form-add-post").addEventListener('submit', (e) => {

	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#text-title").value,
		body: document.querySelector("#text-body").value
	});

	count++;

	showPosts(posts);

	alert("Successfully added a new post!");
});

const showPosts = (posts) => {

	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#text-edit-id").value = id;
	document.querySelector("#text-title-edit").value = title;
	document.querySelector("#text-body-edit").value = body;
}

document.querySelector("#form-edit-post").addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector("#text-edit-id").value){
			posts[i].title = document.querySelector("#text-title-edit").value;
			posts[i].body = document.querySelector("#text-body-edit").value;

			showPosts(posts);

			alert("Successfully updated the post!");

			break;
		}
	}
})

const deletePost = (id) => {
	posts  = posts.filter((post) => {
		if(post.id.toString() !== id) {
			return post;
		}
	})

	document.querySelector(`#post-${id}`).remove();
}