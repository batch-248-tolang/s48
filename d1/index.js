// This will hold our post entries
let posts = [];

// This is responsible for the iteration of IDs of each post
let count = 1;


// Adding a new post to the 'posts' array
document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	// For preventing default behavior of the form (which is refreshing the page)
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#text-title").value,
		body: document.querySelector("#text-body").value
	});

	// Adds 1 to the current count for the next post to be added to have a different ID
	count++;

	showPosts(posts);

	alert("Successfully added a new post!");
});


// For showing all of the posts within the 'posts' array
const showPosts = (posts) => {
	// Temporary handler of the string which contains the template of each post
	let postEntries = '';

	// Loops through each post in the 'posts' array and puts their values within an HTML template
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	// Puts the contents of the 'postEntries' variable into the #div-post-entries element in index.html
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// For editing an existing post in the "posts" array

// This gets the values (title body) from the specific post and passes it to the "Edit post" form fields.
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#text-edit-id").value = id;
	document.querySelector("#text-title-edit").value = title;
	document.querySelector("#text-body-edit").value = body;
}

document.querySelector("#form-edit-post").addEventListener('submit', (e) => {
	e.preventDefault();

	// Loops through the whole posts array
	for(let i = 0; i < posts.length; i++){
		// Checks of the post ID of an existing post is equal to the value of the "#text-edit-id" field in the edit form
		if(posts[i].id.toString() === document.querySelector("#text-edit-id").value){
			// Responsible for assigning the new values which will come from the edit form to the existing values of the matched podst in the array
			posts[i].title = document.querySelector("#text-title-edit").value;
			posts[i].body = document.querySelector("#text-body-edit").value;

			showPosts(posts);

			alert("Successfully updated the post!");

			break;
		}
	}
})



// For deleting existing post in the "post" array
const deletePost = (id) => {
	posts  = posts.filter((post) => {
		// Filters the posts array so that it will only show the post that is not equal to the ID of the post that the user has deleted
		if(post.id.toString() !== id) {
			return post;
		}
	})

	// Removes the element in which the ID of the deleted post is attached to
	document.querySelector(`#post-${id}`).remove();
}